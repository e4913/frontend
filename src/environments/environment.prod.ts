import { Environment } from '@abp/ng.core';

const baseUrl = 'http://localhost:4200';

export const environment = {
  production: true,
  application: {
    baseUrl,
    name: 'Events',
    logoUrl: '',
  },
  oAuthConfig: {
    issuer: 'https://events.fisting.tech',
    redirectUri: baseUrl,
    clientId: 'Events_App',
    responseType: 'code',
    scope: 'offline_access Events',
    requireHttps: true
  },
  apis: {
    default: {
      url: 'https://events.fisting.tech',
      rootNamespace: 'Events',
    },
  },
} as Environment;
