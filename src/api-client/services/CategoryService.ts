/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import type { Observable } from 'rxjs';

import type { CategoryDto } from '../models/CategoryDto';
import type { CreateUpdateCategoryDto } from '../models/CreateUpdateCategoryDto';
import type { PagedResultDtoOfCategoryDto } from '../models/PagedResultDtoOfCategoryDto';

import { OpenAPI } from '../core/OpenAPI';
import { request as __request } from '../core/request';

@Injectable()
export class CategoryService {

    constructor(public readonly http: HttpClient) {}

    /**
     * @param requestBody 
     * @returns CategoryDto Success
     * @throws ApiError
     */
    public categoryCreate(
requestBody?: CreateUpdateCategoryDto,
): Observable<CategoryDto> {
        return __request(OpenAPI, this.http, {
            method: 'POST',
            url: '/api/app/category',
            body: requestBody,
            mediaType: 'application/json',
            errors: {
                400: `Bad Request`,
                401: `Unauthorized`,
                403: `Forbidden`,
                404: `Not Found`,
                500: `Server Error`,
                501: `Server Error`,
            },
        });
    }

    /**
     * @param skipCount 
     * @param maxResultCount 
     * @param sorting 
     * @returns PagedResultDtoOfCategoryDto Success
     * @throws ApiError
     */
    public categoryGetList(
skipCount?: number,
maxResultCount?: number,
sorting?: string,
): Observable<PagedResultDtoOfCategoryDto> {
        return __request(OpenAPI, this.http, {
            method: 'GET',
            url: '/api/app/category',
            query: {
                'SkipCount': skipCount,
                'MaxResultCount': maxResultCount,
                'Sorting': sorting,
            },
            errors: {
                400: `Bad Request`,
                401: `Unauthorized`,
                403: `Forbidden`,
                404: `Not Found`,
                500: `Server Error`,
                501: `Server Error`,
            },
        });
    }

    /**
     * @param id 
     * @param requestBody 
     * @returns CategoryDto Success
     * @throws ApiError
     */
    public categoryUpdate(
id: string,
requestBody?: CreateUpdateCategoryDto,
): Observable<CategoryDto> {
        return __request(OpenAPI, this.http, {
            method: 'PUT',
            url: '/api/app/category/{id}',
            path: {
                'id': id,
            },
            body: requestBody,
            mediaType: 'application/json',
            errors: {
                400: `Bad Request`,
                401: `Unauthorized`,
                403: `Forbidden`,
                404: `Not Found`,
                500: `Server Error`,
                501: `Server Error`,
            },
        });
    }

    /**
     * @param id 
     * @returns any Success
     * @throws ApiError
     */
    public categoryDelete(
id: string,
): Observable<any> {
        return __request(OpenAPI, this.http, {
            method: 'DELETE',
            url: '/api/app/category/{id}',
            path: {
                'id': id,
            },
            errors: {
                400: `Bad Request`,
                401: `Unauthorized`,
                403: `Forbidden`,
                404: `Not Found`,
                500: `Server Error`,
                501: `Server Error`,
            },
        });
    }

    /**
     * @param id 
     * @returns CategoryDto Success
     * @throws ApiError
     */
    public categoryGet(
id: string,
): Observable<CategoryDto> {
        return __request(OpenAPI, this.http, {
            method: 'GET',
            url: '/api/app/category/{id}',
            path: {
                'id': id,
            },
            errors: {
                400: `Bad Request`,
                401: `Unauthorized`,
                403: `Forbidden`,
                404: `Not Found`,
                500: `Server Error`,
                501: `Server Error`,
            },
        });
    }

}
