/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import type { Observable } from 'rxjs';

import type { FindTenantResultDto } from '../models/FindTenantResultDto';

import { OpenAPI } from '../core/OpenAPI';
import { request as __request } from '../core/request';

@Injectable()
export class AbpTenantService {

    constructor(public readonly http: HttpClient) {}

    /**
     * @param name 
     * @returns FindTenantResultDto Success
     * @throws ApiError
     */
    public abpTenantFindTenantByName(
name: string,
): Observable<FindTenantResultDto> {
        return __request(OpenAPI, this.http, {
            method: 'GET',
            url: '/api/abp/multi-tenancy/tenants/by-name/{name}',
            path: {
                'name': name,
            },
            errors: {
                400: `Bad Request`,
                401: `Unauthorized`,
                403: `Forbidden`,
                404: `Not Found`,
                500: `Server Error`,
                501: `Server Error`,
            },
        });
    }

    /**
     * @param id 
     * @returns FindTenantResultDto Success
     * @throws ApiError
     */
    public abpTenantFindTenantById(
id: string,
): Observable<FindTenantResultDto> {
        return __request(OpenAPI, this.http, {
            method: 'GET',
            url: '/api/abp/multi-tenancy/tenants/by-id/{id}',
            path: {
                'id': id,
            },
            errors: {
                400: `Bad Request`,
                401: `Unauthorized`,
                403: `Forbidden`,
                404: `Not Found`,
                500: `Server Error`,
                501: `Server Error`,
            },
        });
    }

}
