/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import type { Observable } from 'rxjs';

import type { ListResultDtoOfOrganizerListElementDto } from '../models/ListResultDtoOfOrganizerListElementDto';
import type { OrganizerCreateDto } from '../models/OrganizerCreateDto';
import type { OrganizerDto } from '../models/OrganizerDto';
import type { OrganizerProfileDto } from '../models/OrganizerProfileDto';
import type { OrganizerUpdateDto } from '../models/OrganizerUpdateDto';
import type { PagedResultDtoOfOrganizerListElementDto } from '../models/PagedResultDtoOfOrganizerListElementDto';

import { OpenAPI } from '../core/OpenAPI';
import { request as __request } from '../core/request';

@Injectable()
export class OrganizerService {

    constructor(public readonly http: HttpClient) {}

    /**
     * @param requestBody 
     * @returns OrganizerDto Success
     * @throws ApiError
     */
    public organizerCreate(
requestBody?: OrganizerCreateDto,
): Observable<OrganizerDto> {
        return __request(OpenAPI, this.http, {
            method: 'POST',
            url: '/api/app/organizer',
            body: requestBody,
            mediaType: 'application/json',
            errors: {
                400: `Bad Request`,
                401: `Unauthorized`,
                403: `Forbidden`,
                404: `Not Found`,
                500: `Server Error`,
                501: `Server Error`,
            },
        });
    }

    /**
     * @param ownerId 
     * @param name 
     * @param skipCount 
     * @param maxResultCount 
     * @returns PagedResultDtoOfOrganizerListElementDto Success
     * @throws ApiError
     */
    public organizerGetList(
ownerId?: string,
name?: string,
skipCount?: number,
maxResultCount?: number,
): Observable<PagedResultDtoOfOrganizerListElementDto> {
        return __request(OpenAPI, this.http, {
            method: 'GET',
            url: '/api/app/organizer',
            query: {
                'OwnerId': ownerId,
                'Name': name,
                'SkipCount': skipCount,
                'MaxResultCount': maxResultCount,
            },
            errors: {
                400: `Bad Request`,
                401: `Unauthorized`,
                403: `Forbidden`,
                404: `Not Found`,
                500: `Server Error`,
                501: `Server Error`,
            },
        });
    }

    /**
     * @param name 
     * @returns OrganizerProfileDto Success
     * @throws ApiError
     */
    public organizerGetProfileDto(
name?: string,
): Observable<OrganizerProfileDto> {
        return __request(OpenAPI, this.http, {
            method: 'GET',
            url: '/api/app/organizer/profile-dto',
            query: {
                'name': name,
            },
            errors: {
                400: `Bad Request`,
                401: `Unauthorized`,
                403: `Forbidden`,
                404: `Not Found`,
                500: `Server Error`,
                501: `Server Error`,
            },
        });
    }

    /**
     * @param userId 
     * @returns ListResultDtoOfOrganizerListElementDto Success
     * @throws ApiError
     */
    public organizerGetOrganizersByUserId(
userId: string,
): Observable<ListResultDtoOfOrganizerListElementDto> {
        return __request(OpenAPI, this.http, {
            method: 'GET',
            url: '/api/app/organizer/organizers-by-user-id/{userId}',
            path: {
                'userId': userId,
            },
            errors: {
                400: `Bad Request`,
                401: `Unauthorized`,
                403: `Forbidden`,
                404: `Not Found`,
                500: `Server Error`,
                501: `Server Error`,
            },
        });
    }

    /**
     * @param organizerId 
     * @returns boolean Success
     * @throws ApiError
     */
    public organizerIsOrganizerOwner(
organizerId: string,
): Observable<boolean> {
        return __request(OpenAPI, this.http, {
            method: 'POST',
            url: '/api/app/organizer/is-organizer-owner/{organizerId}',
            path: {
                'organizerId': organizerId,
            },
            errors: {
                400: `Bad Request`,
                401: `Unauthorized`,
                403: `Forbidden`,
                404: `Not Found`,
                500: `Server Error`,
                501: `Server Error`,
            },
        });
    }

    /**
     * @param id 
     * @param requestBody 
     * @returns any Success
     * @throws ApiError
     */
    public organizerUpdate(
id: string,
requestBody?: OrganizerUpdateDto,
): Observable<any> {
        return __request(OpenAPI, this.http, {
            method: 'PUT',
            url: '/api/app/organizer/{id}',
            path: {
                'id': id,
            },
            body: requestBody,
            mediaType: 'application/json',
            errors: {
                400: `Bad Request`,
                401: `Unauthorized`,
                403: `Forbidden`,
                404: `Not Found`,
                500: `Server Error`,
                501: `Server Error`,
            },
        });
    }

}
