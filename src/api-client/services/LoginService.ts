/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import type { Observable } from 'rxjs';

import type { AbpLoginResult } from '../models/AbpLoginResult';
import type { UserLoginInfo } from '../models/UserLoginInfo';

import { OpenAPI } from '../core/OpenAPI';
import { request as __request } from '../core/request';

@Injectable()
export class LoginService {

    constructor(public readonly http: HttpClient) {}

    /**
     * @param requestBody 
     * @returns AbpLoginResult Success
     * @throws ApiError
     */
    public loginLogin(
requestBody?: UserLoginInfo,
): Observable<AbpLoginResult> {
        return __request(OpenAPI, this.http, {
            method: 'POST',
            url: '/api/account/login',
            body: requestBody,
            mediaType: 'application/json',
            errors: {
                400: `Bad Request`,
                401: `Unauthorized`,
                403: `Forbidden`,
                404: `Not Found`,
                500: `Server Error`,
                501: `Server Error`,
            },
        });
    }

    /**
     * @returns any Success
     * @throws ApiError
     */
    public loginLogout(): Observable<any> {
        return __request(OpenAPI, this.http, {
            method: 'GET',
            url: '/api/account/logout',
            errors: {
                400: `Bad Request`,
                401: `Unauthorized`,
                403: `Forbidden`,
                404: `Not Found`,
                500: `Server Error`,
                501: `Server Error`,
            },
        });
    }

    /**
     * @param requestBody 
     * @returns AbpLoginResult Success
     * @throws ApiError
     */
    public loginCheckPassword(
requestBody?: UserLoginInfo,
): Observable<AbpLoginResult> {
        return __request(OpenAPI, this.http, {
            method: 'POST',
            url: '/api/account/check-password',
            body: requestBody,
            mediaType: 'application/json',
            errors: {
                400: `Bad Request`,
                401: `Unauthorized`,
                403: `Forbidden`,
                404: `Not Found`,
                500: `Server Error`,
                501: `Server Error`,
            },
        });
    }

}
