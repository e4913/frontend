/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import type { Observable } from 'rxjs';

import type { PagedResultDtoOfOrganizerEmployeeDto } from '../models/PagedResultDtoOfOrganizerEmployeeDto';

import { OpenAPI } from '../core/OpenAPI';
import { request as __request } from '../core/request';

@Injectable()
export class OrganizerEmployeeService {

    constructor(public readonly http: HttpClient) {}

    /**
     * @param organizerId 
     * @returns any Success
     * @throws ApiError
     */
    public organizerEmployeeJoin(
organizerId: string,
): Observable<any> {
        return __request(OpenAPI, this.http, {
            method: 'POST',
            url: '/api/app/organizer-employee/join/{organizerId}',
            path: {
                'organizerId': organizerId,
            },
            errors: {
                400: `Bad Request`,
                401: `Unauthorized`,
                403: `Forbidden`,
                404: `Not Found`,
                500: `Server Error`,
                501: `Server Error`,
            },
        });
    }

    /**
     * @param organizerId 
     * @returns any Success
     * @throws ApiError
     */
    public organizerEmployeeLeave(
organizerId: string,
): Observable<any> {
        return __request(OpenAPI, this.http, {
            method: 'POST',
            url: '/api/app/organizer-employee/leave/{organizerId}',
            path: {
                'organizerId': organizerId,
            },
            errors: {
                400: `Bad Request`,
                401: `Unauthorized`,
                403: `Forbidden`,
                404: `Not Found`,
                500: `Server Error`,
                501: `Server Error`,
            },
        });
    }

    /**
     * @param organizerId 
     * @returns boolean Success
     * @throws ApiError
     */
    public organizerEmployeeIsJoined(
organizerId: string,
): Observable<boolean> {
        return __request(OpenAPI, this.http, {
            method: 'POST',
            url: '/api/app/organizer-employee/is-joined/{organizerId}',
            path: {
                'organizerId': organizerId,
            },
            errors: {
                400: `Bad Request`,
                401: `Unauthorized`,
                403: `Forbidden`,
                404: `Not Found`,
                500: `Server Error`,
                501: `Server Error`,
            },
        });
    }

    /**
     * @param organizerId 
     * @param skipCount 
     * @param maxResultCount 
     * @returns PagedResultDtoOfOrganizerEmployeeDto Success
     * @throws ApiError
     */
    public organizerEmployeeGetEmployers(
organizerId?: string,
skipCount?: number,
maxResultCount?: number,
): Observable<PagedResultDtoOfOrganizerEmployeeDto> {
        return __request(OpenAPI, this.http, {
            method: 'GET',
            url: '/api/app/organizer-employee/employers',
            query: {
                'OrganizerId': organizerId,
                'SkipCount': skipCount,
                'MaxResultCount': maxResultCount,
            },
            errors: {
                400: `Bad Request`,
                401: `Unauthorized`,
                403: `Forbidden`,
                404: `Not Found`,
                500: `Server Error`,
                501: `Server Error`,
            },
        });
    }

}
