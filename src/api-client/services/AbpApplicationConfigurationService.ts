/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import type { Observable } from 'rxjs';

import type { ApplicationConfigurationDto } from '../models/ApplicationConfigurationDto';

import { OpenAPI } from '../core/OpenAPI';
import { request as __request } from '../core/request';

@Injectable()
export class AbpApplicationConfigurationService {

    constructor(public readonly http: HttpClient) {}

    /**
     * @returns ApplicationConfigurationDto Success
     * @throws ApiError
     */
    public abpApplicationConfigurationGet(): Observable<ApplicationConfigurationDto> {
        return __request(OpenAPI, this.http, {
            method: 'GET',
            url: '/api/abp/application-configuration',
            errors: {
                400: `Bad Request`,
                401: `Unauthorized`,
                403: `Forbidden`,
                404: `Not Found`,
                500: `Server Error`,
                501: `Server Error`,
            },
        });
    }

}
