/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import type { Observable } from 'rxjs';

import type { ApplicationApiDescriptionModel } from '../models/ApplicationApiDescriptionModel';

import { OpenAPI } from '../core/OpenAPI';
import { request as __request } from '../core/request';

@Injectable()
export class AbpApiDefinitionService {

    constructor(public readonly http: HttpClient) {}

    /**
     * @param includeTypes 
     * @returns ApplicationApiDescriptionModel Success
     * @throws ApiError
     */
    public abpApiDefinitionGet(
includeTypes?: boolean,
): Observable<ApplicationApiDescriptionModel> {
        return __request(OpenAPI, this.http, {
            method: 'GET',
            url: '/api/abp/api-definition',
            query: {
                'IncludeTypes': includeTypes,
            },
            errors: {
                400: `Bad Request`,
                401: `Unauthorized`,
                403: `Forbidden`,
                404: `Not Found`,
                500: `Server Error`,
                501: `Server Error`,
            },
        });
    }

}
