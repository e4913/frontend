/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import type { Observable } from 'rxjs';

import type { GetFeatureListResultDto } from '../models/GetFeatureListResultDto';
import type { UpdateFeaturesDto } from '../models/UpdateFeaturesDto';

import { OpenAPI } from '../core/OpenAPI';
import { request as __request } from '../core/request';

@Injectable()
export class FeaturesService {

    constructor(public readonly http: HttpClient) {}

    /**
     * @param providerName 
     * @param providerKey 
     * @returns GetFeatureListResultDto Success
     * @throws ApiError
     */
    public featuresGet(
providerName?: string,
providerKey?: string,
): Observable<GetFeatureListResultDto> {
        return __request(OpenAPI, this.http, {
            method: 'GET',
            url: '/api/feature-management/features',
            query: {
                'providerName': providerName,
                'providerKey': providerKey,
            },
            errors: {
                400: `Bad Request`,
                401: `Unauthorized`,
                403: `Forbidden`,
                404: `Not Found`,
                500: `Server Error`,
                501: `Server Error`,
            },
        });
    }

    /**
     * @param providerName 
     * @param providerKey 
     * @param requestBody 
     * @returns any Success
     * @throws ApiError
     */
    public featuresUpdate(
providerName?: string,
providerKey?: string,
requestBody?: UpdateFeaturesDto,
): Observable<any> {
        return __request(OpenAPI, this.http, {
            method: 'PUT',
            url: '/api/feature-management/features',
            query: {
                'providerName': providerName,
                'providerKey': providerKey,
            },
            body: requestBody,
            mediaType: 'application/json',
            errors: {
                400: `Bad Request`,
                401: `Unauthorized`,
                403: `Forbidden`,
                404: `Not Found`,
                500: `Server Error`,
                501: `Server Error`,
            },
        });
    }

}
