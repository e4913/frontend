/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import type { Observable } from 'rxjs';

import type { IdentityRoleCreateDto } from '../models/IdentityRoleCreateDto';
import type { IdentityRoleDto } from '../models/IdentityRoleDto';
import type { IdentityRoleUpdateDto } from '../models/IdentityRoleUpdateDto';
import type { ListResultDtoOfIdentityRoleDto } from '../models/ListResultDtoOfIdentityRoleDto';
import type { PagedResultDtoOfIdentityRoleDto } from '../models/PagedResultDtoOfIdentityRoleDto';

import { OpenAPI } from '../core/OpenAPI';
import { request as __request } from '../core/request';

@Injectable()
export class RoleService {

    constructor(public readonly http: HttpClient) {}

    /**
     * @returns ListResultDtoOfIdentityRoleDto Success
     * @throws ApiError
     */
    public roleGetAllList(): Observable<ListResultDtoOfIdentityRoleDto> {
        return __request(OpenAPI, this.http, {
            method: 'GET',
            url: '/api/identity/roles/all',
            errors: {
                400: `Bad Request`,
                401: `Unauthorized`,
                403: `Forbidden`,
                404: `Not Found`,
                500: `Server Error`,
                501: `Server Error`,
            },
        });
    }

    /**
     * @param filter 
     * @param sorting 
     * @param skipCount 
     * @param maxResultCount 
     * @returns PagedResultDtoOfIdentityRoleDto Success
     * @throws ApiError
     */
    public roleGetList(
filter?: string,
sorting?: string,
skipCount?: number,
maxResultCount?: number,
): Observable<PagedResultDtoOfIdentityRoleDto> {
        return __request(OpenAPI, this.http, {
            method: 'GET',
            url: '/api/identity/roles',
            query: {
                'Filter': filter,
                'Sorting': sorting,
                'SkipCount': skipCount,
                'MaxResultCount': maxResultCount,
            },
            errors: {
                400: `Bad Request`,
                401: `Unauthorized`,
                403: `Forbidden`,
                404: `Not Found`,
                500: `Server Error`,
                501: `Server Error`,
            },
        });
    }

    /**
     * @param requestBody 
     * @returns IdentityRoleDto Success
     * @throws ApiError
     */
    public roleCreate(
requestBody?: IdentityRoleCreateDto,
): Observable<IdentityRoleDto> {
        return __request(OpenAPI, this.http, {
            method: 'POST',
            url: '/api/identity/roles',
            body: requestBody,
            mediaType: 'application/json',
            errors: {
                400: `Bad Request`,
                401: `Unauthorized`,
                403: `Forbidden`,
                404: `Not Found`,
                500: `Server Error`,
                501: `Server Error`,
            },
        });
    }

    /**
     * @param id 
     * @returns IdentityRoleDto Success
     * @throws ApiError
     */
    public roleGet(
id: string,
): Observable<IdentityRoleDto> {
        return __request(OpenAPI, this.http, {
            method: 'GET',
            url: '/api/identity/roles/{id}',
            path: {
                'id': id,
            },
            errors: {
                400: `Bad Request`,
                401: `Unauthorized`,
                403: `Forbidden`,
                404: `Not Found`,
                500: `Server Error`,
                501: `Server Error`,
            },
        });
    }

    /**
     * @param id 
     * @param requestBody 
     * @returns IdentityRoleDto Success
     * @throws ApiError
     */
    public roleUpdate(
id: string,
requestBody?: IdentityRoleUpdateDto,
): Observable<IdentityRoleDto> {
        return __request(OpenAPI, this.http, {
            method: 'PUT',
            url: '/api/identity/roles/{id}',
            path: {
                'id': id,
            },
            body: requestBody,
            mediaType: 'application/json',
            errors: {
                400: `Bad Request`,
                401: `Unauthorized`,
                403: `Forbidden`,
                404: `Not Found`,
                500: `Server Error`,
                501: `Server Error`,
            },
        });
    }

    /**
     * @param id 
     * @returns any Success
     * @throws ApiError
     */
    public roleDelete(
id: string,
): Observable<any> {
        return __request(OpenAPI, this.http, {
            method: 'DELETE',
            url: '/api/identity/roles/{id}',
            path: {
                'id': id,
            },
            errors: {
                400: `Bad Request`,
                401: `Unauthorized`,
                403: `Forbidden`,
                404: `Not Found`,
                500: `Server Error`,
                501: `Server Error`,
            },
        });
    }

}
