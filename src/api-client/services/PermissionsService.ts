/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import type { Observable } from 'rxjs';

import type { GetPermissionListResultDto } from '../models/GetPermissionListResultDto';
import type { UpdatePermissionsDto } from '../models/UpdatePermissionsDto';

import { OpenAPI } from '../core/OpenAPI';
import { request as __request } from '../core/request';

@Injectable()
export class PermissionsService {

    constructor(public readonly http: HttpClient) {}

    /**
     * @param providerName 
     * @param providerKey 
     * @returns GetPermissionListResultDto Success
     * @throws ApiError
     */
    public permissionsGet(
providerName?: string,
providerKey?: string,
): Observable<GetPermissionListResultDto> {
        return __request(OpenAPI, this.http, {
            method: 'GET',
            url: '/api/permission-management/permissions',
            query: {
                'providerName': providerName,
                'providerKey': providerKey,
            },
            errors: {
                400: `Bad Request`,
                401: `Unauthorized`,
                403: `Forbidden`,
                404: `Not Found`,
                500: `Server Error`,
                501: `Server Error`,
            },
        });
    }

    /**
     * @param providerName 
     * @param providerKey 
     * @param requestBody 
     * @returns any Success
     * @throws ApiError
     */
    public permissionsUpdate(
providerName?: string,
providerKey?: string,
requestBody?: UpdatePermissionsDto,
): Observable<any> {
        return __request(OpenAPI, this.http, {
            method: 'PUT',
            url: '/api/permission-management/permissions',
            query: {
                'providerName': providerName,
                'providerKey': providerKey,
            },
            body: requestBody,
            mediaType: 'application/json',
            errors: {
                400: `Bad Request`,
                401: `Unauthorized`,
                403: `Forbidden`,
                404: `Not Found`,
                500: `Server Error`,
                501: `Server Error`,
            },
        });
    }

}
