/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type OrganizerEmployeeDto = {
    id?: string;
    employeeUserName?: string | null;
};
