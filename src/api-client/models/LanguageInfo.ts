/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type LanguageInfo = {
    readonly cultureName?: string | null;
    readonly uiCultureName?: string | null;
    readonly displayName?: string | null;
    flagIcon?: string | null;
};
