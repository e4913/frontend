/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type LocalizableStringDto = {
    readonly name?: string | null;
    resource?: string | null;
};
