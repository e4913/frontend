/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type CreateUpdateCategoryDto = {
    name?: string | null;
};
