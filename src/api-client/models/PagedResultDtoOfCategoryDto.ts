/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { CategoryDto } from './CategoryDto';

export type PagedResultDtoOfCategoryDto = {
    items?: Array<CategoryDto> | null;
    totalCount?: number;
};
