/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type CategoryDto = {
    id?: string;
    name?: string | null;
};
