/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type OrganizerProfileDto = {
    id?: string;
    name?: string | null;
    description?: string | null;
    ownerUserName?: string | null;
    ownerEmail?: string | null;
};
