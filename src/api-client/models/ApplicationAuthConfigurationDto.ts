/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type ApplicationAuthConfigurationDto = {
    policies?: Record<string, boolean> | null;
    grantedPolicies?: Record<string, boolean> | null;
};
