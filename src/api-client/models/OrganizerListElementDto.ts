/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type OrganizerListElementDto = {
    id?: string;
    name?: string | null;
    description?: string | null;
};
