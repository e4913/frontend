/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { OrganizerListElementDto } from './OrganizerListElementDto';

export type PagedResultDtoOfOrganizerListElementDto = {
    items?: Array<OrganizerListElementDto> | null;
    totalCount?: number;
};
