/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { OrganizerEmployeeDto } from './OrganizerEmployeeDto';

export type PagedResultDtoOfOrganizerEmployeeDto = {
    items?: Array<OrganizerEmployeeDto> | null;
    totalCount?: number;
};
