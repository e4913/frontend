/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type OrganizerDto = {
    id?: string;
    name?: string | null;
};
