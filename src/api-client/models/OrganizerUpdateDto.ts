/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type OrganizerUpdateDto = {
    name: string;
    description: string;
};
