/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type OrganizerCreateDto = {
    name: string;
    description: string;
};
