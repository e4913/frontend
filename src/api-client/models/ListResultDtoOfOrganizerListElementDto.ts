/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { OrganizerListElementDto } from './OrganizerListElementDto';

export type ListResultDtoOfOrganizerListElementDto = {
    items?: Array<OrganizerListElementDto> | null;
};
